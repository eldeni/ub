
<!-- gnb
    ======================================= -->
<div class="gnb border-black">
    <style scoped>
	    .gnb {
			margin: 0px auto;
			width : 900px;
		    height: 60px;
	    }

	    .gnb .logo {
		    height:60px;
		    display: block;
		    float: left;
		    line-height: 60px;
	    }

		.gnb .search {
			margin-left: 50px;
			float: left;
		}

		.gnb input {
			width: 400px;
			height: 40px;
		    padding: 0px 5px;
		    line-height: 40px;
		    margin-top: 10px;
			/*background: url(../img/index.jpg) no-repeat;*/
		}

		.gnb .account {
			float: left;
			margin-left: 120px;
		}

		.gnb .btn {
		    vertical-align: middle;
		    font-size: 1.0em;
		    margin: 10px 5px 0px 5px;
	        padding : 0px;
		    width: 40px;
		    height : 40px;
		    border:1px solid #2D79B2;
		    line-height : 40px;
		    text-align : center;
		    -webkit-border-radius : 5px;
		    -moz-border-radius : 5px;
		    border-radius : 5px;
		    cursor : pointer;
	    }

		.gnb .btn:hover{
		    background : #74C3FF;
	    }

		.gnb .account_panel{
			width : 150px;
			height : 150px;
			border:1px solid #2D79B2;
			border-radius : 5px;
			position : absolute;
			display: none;
		}

    </style>

	<?php
        if(isset($_SESSION["member"])) $member = $_SESSION["member"];
        else $member = null;
    ?>

	<?php
		//test
		if(!empty($member)) echo $member['nickname'], " logged in";
	?>

    <a class="logo btn border-black">UB</a>
	<div class="search border-black">
		<input type="text" placeholder="검색어를 입력하세요." class="">
		<button type="button" class="btn">sch</button>
		<a href="#" class="btn search-random">rnd</a></li>
	</div>
	<div class="account border-yellow">
		<a href="#" class="btn">+</a>
		<a href="#" class="btn">acc</a>
	</div>




	<!-- hidden div -->
	<div class="account_panel">
		account_panel
	</div>

</div>
<!-- gnb
    ======================================= -->

<script>
	(function(window, document){
		//Event Handling
		$( '.sign-up' ).on( 'click' , function(){
			location.href = '/member?action=create';
		});
		$( '.login' ).on( 'click' , function(){
			location.href = '/login';
		});
		$( '.logout' ).on( 'click' , function(){
			location.href = '/logout';
		});
		$( '.add-term' ).on( 'click' , function(){
			location.href='/addTerm';
		});
		$( '.account' ).on( 'click' , function(){
			$('.account_box').show().position({
				my : 'right top',
				at : 'right bottom',
				of : '.account'
			});
		});
	})(window, document);

</script>
