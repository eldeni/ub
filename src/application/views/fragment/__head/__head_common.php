<?php
/**
 * Common fraction in head element.
 */

?>
<meta charset="UTF-8">

<link rel="stylesheet" type="text/css" href="/application/views/css/normalize.css">
<link rel="stylesheet" type="text/css" href="/application/views/css/common.css">

<script src="/application/views/lib/jquery-1.11.3.min.js"></script>
<script src="/application/views/lib/modernizr-2.8.3.min.js"></script>

<!-- User design -->
<script src="/application/views/js/main.js"></script>

<!-- For designing user-interface -->
<link rel="stylesheet" type="text/css" href="/application/views/lib/jquery-ui.min.css">
<script src="/application/views/lib/jquery-ui.min.js"></script>