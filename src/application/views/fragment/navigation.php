
<!-- navigation
    ======================================= -->
<div class="navigation border-blue">
	<style scoped>
		* {
			margin: 0px;
			padding: 0px;
			text-decoration: none;
			list-style: none;
		}

		.navigation {
			border-bottom: 1px solid gray;
			padding: 10px 0px;
		}

		.navigation a {
			display: block;
		}

		.trigger {

		}

	</style>
	<div class="switch border-black">drop switch</div>
	<div class="dropped border-black">
		<ul>1
			<li><a href="" class="about border-gray">about</a></li>
			<li><a href="" class="notice border-gray">notice</a></li>
			<li><a href="" class="rules border-gray">rules</a></li>
			<li><a href="" class="guide border-gray">guide</a></li>
			<li><a href="" class="terms border-gray">terms of service</a></li>
			<li><a href="" class="privacy border-gray">privacy</a></li>
			<li><a href="" class="feedback border-gray">feedback</a></li>
		</ul>
	</div>

</div>
<!-- //navigation
    ======================================= -->